package com.example.android.quakereport;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class EarthquakeAdapter extends ArrayAdapter<Earthquake> {
/*   This is custom constructor (it doesn't mirror a superclass constructor).
     The context is used to inflate the layout file, and the list is the data we want
     to populate into the lists.*/

    public EarthquakeAdapter(Activity context, ArrayList<Earthquake> earthquakeArrayList) {
/*      Initialize the ArrayAdapter's internal storage for the context and the list.
        The second argument is used when the ArrayAdapter is populating a single TextView.
        Because this is a custom adapter for three TextViews , the adapter is not
        going to use this second argument, so it can be any value. Here, we used 0.*/
        super(context, 0, earthquakeArrayList);
    }

  /*This method provides a view for an AdapterView
    position - the position in the list of data that should be displayed in the list item view
    convertView - the rebuild view to populate
    parent - the parent ViewGroup that is used for inflation*/

    public View getView(int position, View convertView, ViewGroup parent) {
        // check View for emptiness
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.earthquake, parent, false);
        }

        // Get the Earthquake object located at this position in the list
        Earthquake currentEarthquake = getItem(position);
        // Find the TextView in the Earthquake.xml layout with the ID
        TextView magnitudeTextView = (TextView) listItemView.findViewById(R.id.mag);
        magnitudeTextView.setText(currentEarthquake.getMagnitude());

        TextView placeTextView = (TextView) listItemView.findViewById(R.id.place);
        // Get the place from the current Earthquake object and
        // set this text on the name TextView
        placeTextView.setText(currentEarthquake.getLocation());

        Date dateObject = new Date(currentEarthquake.getDate());
        // Display earthquake date
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM DD, yyyy");
        String dateToDisplay = dateFormatter.format(dateObject);
        TextView dateTextView = (TextView) listItemView.findViewById(R.id.date);
        dateTextView.setText(dateToDisplay);
        // Display earthquake time
        SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
        String timeToDisplay = timeFormatter.format(dateObject);
        TextView timeTextView = (TextView) listItemView.findViewById(R.id.time);
        timeTextView.setText(timeToDisplay);

        return listItemView;

    }
}