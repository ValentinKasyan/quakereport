/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.quakereport;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;


public class EarthquakeActivity extends AppCompatActivity {


    private String TAG = EarthquakeActivity.class.getSimpleName();
    private ListView earthquakeListView;
    ArrayList<HashMap<String, String>> dataList;
    ArrayList<Earthquake> earthquakeArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.earthquake_activity);

        //fill with data from the static method QueryUtils.extractEarthquakes()
        //which returns a collection
        earthquakeArrayList = QueryUtils.extractEarthquakes();

        //we will store data
        dataList = new ArrayList<>();
        // Find a reference to the ListView in the layout
        earthquakeListView = (ListView) findViewById(R.id.list);


        // Create a new  ArrayAdapter of earthquakes
        EarthquakeAdapter adapter = new EarthquakeAdapter(
                this, earthquakeArrayList);

        // Set the adapter on the  ListView
        // so the list can be populated in the user interface
        earthquakeListView.setAdapter(adapter);

    }

}

